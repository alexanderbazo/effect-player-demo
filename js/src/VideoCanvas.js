var App = App || {};
App.VideoCanvas = function(options) {
  "use strict";
  /* eslint-env browser */
  /* global EventPublisher, CanvasFilter */

  var that = new EventPublisher(),
    filter = new CanvasFilter(options.target),
    currentFilter,
    context = options.target.getContext("2d");

  function drawVideoFrame(video) {
    context.drawImage(video, 0, 0, options.target.width, options.target.height);
    if (currentFilter) {
      filter.apply(currentFilter);
    }
  }

  function setFilter(filter) {
    switch (filter) {
      case "grayscale":
        currentFilter = CanvasFilter.GRAYSCALE;
        break;
      case "threshold":
        currentFilter = CanvasFilter.THRESHOLD;
        break;
      case "brighten":
        currentFilter = CanvasFilter.BRIGHTEN;
        break;
      default:
        break;
    }
  }

  function clearFilter() {
    currentFilter = undefined;
  }

  that.drawVideoFrame = drawVideoFrame;
  that.setFilter = setFilter;
  that.clearFilter = clearFilter;
  return that;
};
