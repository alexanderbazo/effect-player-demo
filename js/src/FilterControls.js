var App = App || {};
App.FilterControls = function(options) {
  "use strict";
  /* eslint-env browser */

  var that = new App.Controls(options);

  function init() {
    options.grayscale.classList.add("filter-controls-grayscale-button");
    options.grayscale.addEventListener("click", that.onButtonClicked);
    options.brighten.classList.add("filter-controls-brighten-button");
    options.brighten.addEventListener("click", that.onButtonClicked);
    options.threshold.classList.add("filter-controls-threshold-button");
    options.threshold.addEventListener("click", that.onButtonClicked);
  }

  init();
  return that;
};
